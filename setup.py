#!/usr/bin/env python

from distutils.core import setup

setup(name='mqttlogger',
      version='0.1.0',
      description='Micropython class MqttLogHandler that allows to send logs sending logs to given mqtt topic',
      author='Jan Klusáček',
      author_email='honza.klugmail.com',
      url='https://gitlab.com/users/honza.klu',
      packages=['mqttlogger'],
     )