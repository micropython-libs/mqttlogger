import time
import json
import logging

DEF_LOG_BUFF_SIZE = 1024
LOG_SEND_NOW_PRIORITY = logging.ERROR
MAX_SEND_INTERVAL = 5

logger = logging.getLogger(__name__)

class MqttLogHandler(logging.Handler):
    def __init__(self, mqtt_topic, mqtt_mgr, backup_record=None, buff_size=DEF_LOG_BUFF_SIZE,
                 send_now_priority=LOG_SEND_NOW_PRIORITY, max_send_interval=MAX_SEND_INTERVAL):
        self.mqtt_topic = mqtt_topic
        self.mqtt_mgr = mqtt_mgr # TODO: Make in work with any callback?
        self.backup_record = backup_record
        self.buffer = bytearray(buff_size)
        self.send_now_priority = send_now_priority
        self.max_send_interval = max_send_interval
        self.buffer_ptr = 0
        self.discarded_cnt = 0
        self.last_send = time.time()

    def send_all(self) -> bool:
        if self.mqtt_mgr.mqtt_online:
            self.mqtt_mgr.publish(self.mqtt_topic, self.buffer[0:self.buffer_ptr], qos=0)
            self.buffer_ptr = 0
            self.discarded_cnt = 0
            self.last_send = time.time()
            return True
        return False

    def process_msg(self, msg, priority):
        # print(f"LOG_BUFF {self.buffer_ptr}/{len(self.buffer)}")
        # We have to be very careful where we call logger in this method (maybe do not call it at all?)
        enc_msg = msg.encode('utf8')
        if len(enc_msg)>len(self.buffer):
            logger.error(f"Message is to long for log {len(enc_msg)}/{len(self.buffer)}")
            return
        if self.buffer_ptr+len(enc_msg) >= len(self.buffer):
            if not self.send_all():
                if self.backup_record:
                    self.backup_record(self.buffer, self.buffer_ptr)
                self.discarded_cnt += self.buffer_ptr
                self.buffer_ptr = 0
                logger.error(f"Log buffer is full had to discard {self.discarded_cnt}b")
            return
        self.buffer[self.buffer_ptr:self.buffer_ptr+len(enc_msg)] = enc_msg
        self.buffer_ptr += len(enc_msg)
        if priority>=self.send_now_priority:
            self.send_all()
            return
        if time.time()-self.last_send > self.max_send_interval:
            print("Log send timeout")
            self.send_all()

    def emit(self, d):
        msg = json.dumps({"name": d.name, "level": d.levelname, "message": d.message,
                          "time": (time.time_ns() // 1_000_000) }) + "\n"
        self.process_msg(msg, d.levelno)
