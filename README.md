# mqttlogger

Micropython class MqttLogHandler that allows to send logs sending logs to given mqtt topic.
It requires MqttManager

## Example
mqtt_log_handler = MqttLogHandler(f'log', mqtt_mgr)